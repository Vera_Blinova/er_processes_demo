#include "entitynotfoundexception.h"


EntityNotFoundException::EntityNotFoundException(string _entityName) : Exception(){
    entityName = _entityName;
    message = "EntityNotFoundException: Сущность " + _entityName + " не найдена";
}

const char *EntityNotFoundException::what() const throw()
{
    return message.c_str();
}
