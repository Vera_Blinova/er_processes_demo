#ifndef ENTITYEXISTEXCEPTION_H
#define ENTITYEXISTEXCEPTION_H


#include <string>
#include "Exception.h"

using namespace std;

class EntityExistException : public Exception
{
private:
    string entityName;
    string message;
public:
    EntityExistException(string text);

    string EntityName() { return entityName; }
    string Message() { return message; }

    virtual const char* what() const throw();
};

#endif // ENTITYEXISTEXCEPTION_H
