#include "Exception.h"

Exception::Exception() : exception()
{
    fullString = "";
}

Exception::Exception(string text) : exception(), fullString(text)
{
    
}

const char *Exception::what() const throw()
{
    return fullString.c_str();
}
