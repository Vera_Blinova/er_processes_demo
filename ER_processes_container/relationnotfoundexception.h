#ifndef RELATIONNOTFOUNDEXCEPTION_H
#define RELATIONNOTFOUNDEXCEPTION_H

#include <string>
#include "Exception.h"

using namespace std;

class RelationNotFoundException : public Exception
{
private:
    string relationName;
    string message;
public:
    RelationNotFoundException(string text);

    string RelationName() { return relationName; }
    string Message() { return message; }

    virtual const char* what() const throw();
};

#endif // RELATIONNOTFOUNDEXCEPTION_H
