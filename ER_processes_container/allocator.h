#ifndef ALLOCATOR_H
#define ALLOCATOR_H

#include <map>
#include <vector>
#include <string>

using namespace std;

template <typename T>
class Allocator
{
public:
    typedef T value_type;
    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    template<typename U>
    struct rebind
    {
        typedef Allocator<U> other;
    };

    Allocator() = default;
    Allocator(void* p): p_graph(p) {}
    Allocator(const Allocator&) = default;
    template <typename U>
    Allocator(const Allocator<U>&) {}
    ~Allocator() {}

    Allocator& operator = (const Allocator& other) = default;
    pointer allocate(size_type n, const void* hint = 0);
    void deallocate(pointer p, size_type n);

    template<typename U, typename... Args>
    void construct(U* p, Args&&... args);

    template<typename U>
    void destroy(U* p);

private:
    void *p_graph;
};

#include "Allocator.cpp"

#endif // ALLOCATOR_H
