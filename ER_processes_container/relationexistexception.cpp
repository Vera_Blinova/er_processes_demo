#include "relationexistexception.h"

RelationExistException::RelationExistException(string _relationName) : Exception(){
    relationName = _relationName;
    message = "RelationExistException: Отношение " + _relationName + " уже существует";
}

const char *RelationExistException::what() const throw()
{
    return message.c_str();
}
