#ifndef GRAPH_H
#include "graph.h"
#endif
#ifndef _GRAPH_CPP_
#define _GRAPH_CPP_


template <typename T>
Entity<T>* GraphER <T>::AddEntity(string name, T data)
{
    if (isEntitiesContain(name))
        throw EntityExistException(name);
    Entity<T> *entity = new Entity<T>(name, data);
    entities.insert(pair<string, Entity<T>*>(entity->getName(), entity));
    return entity;
}

template <typename T>
Entity<T>* GraphER <T>::GetEntity(string name)
{
    try
    {
        return entities.at(name);
    } catch (out_of_range){
        
        throw EntityNotFoundException(name);
    }
}

template <typename T>
Entity<T>* GraphER <T>::RenameEntity(string oldName, string newName)
{
    typename mapE::const_iterator it;
    Entity<T> *entity = GetEntity(oldName);
    if (entity == NULL)
        throw EntityNotFoundException(oldName);
    if (isEntitiesContain(newName))
        throw EntityExistException(newName);
    //переименование сущности в описаниях отношений
   for (it = entity->Relations_1_1.begin(); it!=entity->Relations_1_1.end();++it)
        if (relations.at(it->first)->Entity2 == oldName)
            relations.at(it->first)->Entity2 = newName;
        else
            relations.at(it->first)->Entity1 = newName;
    
    for (it = entity->Relations_1_M.begin(); it!=entity->Relations_1_M.end();++it)
        relations.at(it->first)->Entity1 = newName;
    
    for (it = entity->Relations_M_1.begin(); it!=entity->Relations_M_1.end();++it)
        relations.at(it->first)->Entity2 = newName;
    
    //переименование самой сущности
    entity->setName(newName);
    entities.erase(oldName);
    entities.insert(pair<string, Entity<T>*>(entity->getName(), entity));
    
    return entity;
}

template <typename T>
bool GraphER <T>::RemoveEntity(string name)
{
    Entity<T> *entity = GetEntity(name);
    //удалить Relations
    vector<string> removingRelations;
    typename mapE::const_iterator it;
    for (it = entity->Relations_1_1.begin();it!= entity->Relations_1_1.end();it++)
        removingRelations.push_back(it->first);
    
    for (it = entity->Relations_1_M.begin();it!= entity->Relations_1_M.end();it++)
        removingRelations.push_back(it->first);
    
    for (it = entity->Relations_M_1.begin();it!= entity->Relations_M_1.end();it++)
        removingRelations.push_back(it->first);
    
    for (string rel : removingRelations)
        RemoveRelation(rel);
    
    //удалить сущность
    entities.erase(name);
    
    return true;
}
template <typename T>
map<string, Entity<T>*> GraphER <T>::GetEntities()
{
    return entities;
//    vector<string> list;
//    for(map<string, Entity<T>*>::const_iterator it = entities.begin();
//        it != entities.end(); ++it)
//    {
//        list.push_back(it->first);
//    }
//    return list;
}

template <typename T>
Relation* GraphER <T>::AddRelation(string name, string entity1Name, string entity2Name, RelTy relationType)
{
    if (isRelationsContain(name))
        throw RelationExistException(name);
    
    Entity<T> *entity1 = GetEntity(entity1Name);
    if (entity1 == NULL)
        throw EntityNotFoundException(entity1Name);
    
    Entity<T> *entity2 = GetEntity(entity2Name);
    if (entity2 == NULL)
        throw EntityNotFoundException(entity2Name);
    
    //вставка нового отношения в словарь отношений
    Relation *relation = new Relation(name, entity1Name, entity2Name, relationType);
    relations.insert(pair<string, Relation*>(relation->Name, relation));
    
    //создание ссылок между entity1 и entity2
    
    if (relationType == RelTy::T_1_1)
    {
        entity1->Relations_1_1.insert(pair<string, Entity<T>*>(relation->Name,entity2));
        entity2->Relations_1_1.insert(pair<string, Entity<T>*>(relation->Name,entity1));
    }
    else if (relationType == RelTy::T_1_M)
    {
        entity1->Relations_1_M.insert(pair<string, Entity<T>*>(relation->Name,entity2));
        entity2->Relations_M_1.insert(pair<string, Entity<T>*>(relation->Name,entity1));
    }
    else
    {
        entity1->Relations_M_1.insert(pair<string, Entity<T>*>(relation->Name,entity2));
        entity2->Relations_1_M.insert(pair<string, Entity<T>*>(relation->Name,entity1));
    }
    return relation;
    
}

template <typename T>
Relation* GraphER <T>::RenameRelation(string oldName, string newName)
{
    Relation *relation = GetRelation(oldName);
    
    if (isRelationsContain(newName))
        throw RelationExistException(newName);
    
    Entity<T> *ent1 = GetEntity(relation->Entity1);
    Entity<T> *ent2 = GetEntity(relation->Entity2);
    typename mapE::iterator it;
    
    map<string,Entity<T>*> *RelationMap1;
    map<string,Entity<T>*> *RelationMap2;
    
    //выбрать тип отношения
    if (relation->Type == RelTy::T_1_1)
    {
        RelationMap1 = &(ent1->Relations_1_1);
        RelationMap2 = &(ent2->Relations_1_1);
        
    }   else
    {
        RelationMap1 = &(ent1->Relations_1_M);
        RelationMap2 = &(ent2->Relations_M_1);
        
    }
    //переименовать ссылки отношения для сущностей
    it = RelationMap1->find(oldName);
    RelationMap1->insert(pair<string,Entity<T>*>(newName,it->second));
    RelationMap1->erase(it);
    
    it = RelationMap2->find(oldName);
    RelationMap2->insert(pair<string,Entity<T>*>(newName,it->second));
    RelationMap2->erase(it);
    
    //переименовать отношение
    relation->Name = newName;
    relations.erase(oldName);
    relations.insert(pair<string, Relation*>(relation->Name, relation));
    
    return relation;
}

template <typename T>
Relation* GraphER <T>::GetRelation(string name)
{
    try
    {
        return relations.at(name);
        
    } catch (out_of_range){
        
        throw RelationNotFoundException(name);
    }
}

template <typename T>
bool GraphER <T>::RemoveRelation(string name)
{
    Relation *relation = GetRelation(name);
    
    //удалить связи между Entity1 и Entity2
    map<string, Entity<T>*> *RelationMap1;
    map<string, Entity<T>*> *RelationMap2;
    if (relation->Type==RelTy::T_1_1)
    {
        RelationMap1 = &(GetEntity(relation->Entity1)->Relations_1_1);
        RelationMap2 = &(GetEntity(relation->Entity2)->Relations_1_1);
    }
    else if (relation->Type==RelTy::T_1_M)
    {
        RelationMap1 = &(GetEntity(relation->Entity1)->Relations_1_M);
        RelationMap2 = &(GetEntity(relation->Entity2)->Relations_M_1);
    }
    else
    {
        RelationMap1 = &(GetEntity(relation->Entity1)->Relations_M_1);
        RelationMap2 = &(GetEntity(relation->Entity2)->Relations_1_M);
    }
    
    RelationMap1->erase(name);
    RelationMap2->erase(name);
    
    //удалить relation
    relations.erase(name);
    
    return true;
}

template <typename T>
map<string, Relation*> GraphER <T>::GetRelations()
{
    return relations;
//    vector<string> list;
//    for(map<string, Relation*>::const_iterator it = relations.begin();
//        it != relations.end(); ++it)
//    {
//        list.push_back(it->first);
//    }
//    return list;
}

template <typename T>
bool GraphER <T>::isEntitiesContain(string name)
{
    if (entities.find(name)==entities.end())
        return false;
    return true;
}

template <typename T>
bool GraphER <T>::isRelationsContain(string name)
{
    if (relations.find(name)==relations.end())
        return false;
    return true;
}

template<typename T>
string GraphER<T>::serialize() const
{
    string str = "{\n \t \"graphName\": \"" + name +"\",\n";
    str += "\t \"entities\": [ \n";
    for(typename mapE::const_iterator it = entities.begin(); it != entities.end(); )
    {
        //запись сущностей
        str += (it->second->serialize());
        if (++it != entities.end())
            str += ',';
        str += '\n';
    }
    if(relations.begin()!=relations.end())
    {
        str += "\t], \n \t\"relations\": [ \n";
        for(map<string, Relation*>::const_iterator it = relations.begin(); it != relations.end();)
        {
            //запись отношений
            str += it->second->serialize();
            if (++it != relations.end())
                str += ',';
            str += '\n';
        }
    }
    str += "\t] \n }";

    return str;
}

template<typename T>
GraphER<T> *GraphER<T>::deserialize(string seria)
{
    if (seria == "")
        return nullptr;
    string pattern = "\"graphName\": \"";
    string sepComma = "\",";
    string sepBracket = "]";
    string list;
    int patternPos = seria.find(pattern);
    seria = seria.substr(patternPos + pattern.length());
    int sepPos = seria.find(sepComma);

    //Создание нового графа
    GraphER<T> *newGraph = new GraphER<T>(seria.substr(0,sepPos));

    //Создание сущностей
    pattern = "\"entities\": [ \n\t\t ";
    patternPos = seria.find(pattern);
    seria = seria.substr(patternPos + pattern.length());
    sepPos = seria.find(sepBracket);
    list = seria.substr(0, sepPos+1);
    seria = seria.substr(sepPos+1);
    while (list.find("\"") != list.npos)
    {

        int startPos = list.find("\"")+1;
        list = list.substr(startPos);
        sepPos = list.find(sepComma);
        if (sepPos == list.npos)
            sepPos = list.find("\"\n");
        string newEntity = list.substr(0,sepPos);
        newGraph->AddEntity(newEntity, "");
        list = list.substr(sepPos+2);

    }


    //Cоздание отношений
    pattern = "\"relations\": [ \n\t\t";
    patternPos = seria.find(pattern);
    seria = seria.substr(patternPos + pattern.length());
    sepPos = seria.find(sepBracket);
    list = seria.substr(0, sepPos+1);
    while (list.find("\"") != list.npos)
    {
        string object = list.substr(list.find("{"),list.find("}") - list.find("{") + 1);
        pattern = "\"name\": \"";
        patternPos = object.find(pattern);
        int startPos = patternPos + pattern.length();
                sepPos = object.find(sepComma);
        string relationName = object.substr(startPos ,sepPos-startPos);
        object = object.substr(sepPos + 1);

        pattern = "\"entity1\": \"";
        patternPos = object.find(pattern);
        startPos = patternPos + pattern.length();
        sepPos = object.find(sepComma);
        string entity1 = object.substr(startPos ,sepPos-startPos);
        object = object.substr(sepPos + 1);

        pattern = "\"entity2\": \"";
        patternPos = object.find(pattern);
        startPos = patternPos + pattern.length();
        sepPos = object.find(sepComma);
        string entity2 = object.substr(startPos ,sepPos-startPos);
        object = object.substr(sepPos + 2);

        pattern = "\"type\": \"";
        patternPos = object.find(pattern);
        startPos = patternPos + pattern.length();
        object = object.substr(startPos);
        sepPos = object.find("\"");
        string type = object.substr(0 ,sepPos);
        object = object.substr(sepPos + 1);

        RelTy relTy;
        if(type == "1:1")
            relTy = RelTy::T_1_1;
        else if (type == "1:M")
            relTy = RelTy::T_1_M;
        else if (type == "M:M")
            relTy = RelTy::T_M_M;
        newGraph->AddRelation(relationName,entity1,entity2,relTy);
        list = list.substr(list.find("}")+1);
    }

    return newGraph;
}

template<typename T>
void GraphER<T>::RenameGraph(string name_)
{
    name = name_;
}

#endif // _GraphER_CPP_


