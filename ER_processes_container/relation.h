#ifndef RELATION_H
#define RELATION_H

#include <string>
#include <map>

using namespace std;

enum RelTy {
    T_1_1,
    T_1_M,
    T_M_M,

};

class Relation
{
   // string _name;
public:
    string Name;
    string Entity1;
    string Entity2;

    RelTy Type;

    Relation(string name,string ent1, string ent2, RelTy type = RelTy::T_1_1);

    string serialize();
    int GetIndexOfType();
    string serializeType();
};

#endif // RELATION_H
