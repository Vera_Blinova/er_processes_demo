#include "entityexistexception.h"


EntityExistException::EntityExistException(string _entityName) : Exception(){
    entityName = _entityName;
    message = "EntityExistException: Сущность " + _entityName + " уже существует";
}

const char *EntityExistException::what() const throw()
{
    return message.c_str();
}
