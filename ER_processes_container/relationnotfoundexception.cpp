#include "relationnotfoundexception.h"

RelationNotFoundException::RelationNotFoundException(string _relationName) : Exception(){
    relationName = _relationName;
    message = "RelationNotFoundException: Отношение " + _relationName + " не найдено";
}

const char *RelationNotFoundException::what() const throw()
{
    return message.c_str();
}
