#ifndef GRAPH_H
#define GRAPH_H

#include <map>
#include <string>
#include <vector>
#include <fstream>

#include "Allocator.h"
#include "entity.h"
#include "relation.h"
#include "entityexistexception.h"
#include "entitynotfoundexception.h"
#include "relationexistexception.h"
#include "relationnotfoundexception.h"

using namespace std;

template <typename T> class GraphER
{
private:
    typedef Entity<T>* EntityTptr;
    typedef map<string, EntityTptr> mapE;

    string name;
    map<string, Entity<T>*> entities;
    map<string, Relation*> relations;
    Allocator<T> myAllocator;

public:
    GraphER()
    {
        name = "graph";
        myAllocator = new Allocator<T>(this);
    }
    GraphER(string _name)
    {
        name = _name;
        myAllocator = new Allocator<T>(this);
    }

    class iterator
    {
    public:
    typedef iterator self_type;
    typedef T value_type;
    typedef T& reference;
    typedef T* pointer;
    typedef forward_iterator_tag iterator_category;
    typedef int difference_type;

    iterator(pointer ptr) : ptr_(ptr) { }

    self_type operator++() { self_type i = *this; ptr_++; return i; }
    self_type operator++(int junk) { ptr_++; return *this; }
    reference operator*() { return *ptr_; }
    pointer operator->() { return ptr_; }
    bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }
    bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }

    private:

    pointer ptr_;
    };

    class const_iterator
    {
    public:

    typedef const_iterator self_type;
    typedef T value_type;
    typedef T& reference;
    typedef T* pointer;
    typedef int difference_type;
    typedef forward_iterator_tag iterator_category;

    const_iterator(pointer ptr) : ptr_(ptr) { }

    self_type operator++() { self_type i = *this; ptr_++; return i; }
    self_type operator++(int junk) { ptr_++; return *this; }
    const reference operator*() { return *ptr_; }
    const pointer operator->() { return ptr_; }
    bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }
    bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }

    private:
    pointer ptr_;
    };


    T* allocate(size_t n) {}
    void deallocate(T* p, size_t n) {}

    iterator begin() { return map<string,Entity<T>*>::iterator(entities.begin()); }
    iterator end() { return map<string,Entity<T>*>::iterator(entities.end()); }

    const_iterator begin() const { return map<string,Entity<T>*>::const_iterator(entities.cbegin()); }
    const_iterator end() const { return map<string,Entity<T>*>::const_iterator(entities.cend()); }

    string serialize() const;
    static GraphER<T>* deserialize(string seria);

    void RenameGraph(string name);
    Entity<T>* AddEntity(string name, T data);
    Entity<T>* GetEntity(string name);
    Entity<T>* RenameEntity(string oldName, string newName);
    bool RemoveEntity(string name);
    map<string,Entity<T>*> GetEntities();

    Relation* AddRelation(string name, string entity1, string entity2, RelTy relationType = RelTy::T_1_1);
    Relation* RenameRelation(string oldName, string newName);
    Relation* GetRelation(string name);
    bool RemoveRelation(string name);
    map<string,Relation*> GetRelations();

    int EntitiesSize() { return entities.size(); }
    int RelationsSize() { return relations.size(); }

    bool isEntitiesContain(string name);
    bool isRelationsContain(string name);

    string getName() {return name;}

};

#include "graph.cpp"

#endif // GRAPH_H
