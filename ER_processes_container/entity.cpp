#ifndef ENTITY_H
#include "entity.h"
#endif

#ifndef ENTITY_CPP
#define ENTITY_CPP

template <typename T>
Entity<T>::Entity(string name, T data)
{
    _name = name;
    value = &data;
}

template <typename T>
Entity<T>::~Entity()
{}

template <typename T>
string Entity<T>::serialize()
{
    string str = "\t\t \"" + getName() +"\"";
    return str;
}

#endif
