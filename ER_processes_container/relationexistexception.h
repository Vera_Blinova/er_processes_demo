#ifndef RELATIONEXISTEXCEPTION_H
#define RELATIONEXISTEXCEPTION_H


#include <string>
#include "Exception.h"

using namespace std;

class RelationExistException : public Exception
{
private:
    string relationName;
    string message;
public:
    RelationExistException(string text);

    string RelationName() { return relationName; }
    string Message() { return message; }

    virtual const char* what() const throw();
};
#endif // RELATIONEXISTEXCEPTION_H
