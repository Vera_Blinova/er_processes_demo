#ifndef ALLOCATOR_H
#include "Allocator.h"
#endif

#ifndef ALLOCATOR_CPP
#define ALLOCATOR_CPP
template<typename T>
typename Allocator<T>::pointer Allocator<T>::allocate(Allocator<T>::size_type n, const void *hint)
{
    p_graph->allocate(n);
}
#endif //ALLOCATOR_CPP



template<typename T>
void Allocator<T>::deallocate(Allocator<T>::pointer p, Allocator<T>::size_type n)
{
    p_graph->deallocate(p, n);
}
