CONFIG += c++14

HEADERS += \
    $$PWD/Allocator.h \
    $$PWD/entity.h \
    $$PWD/entityexistexception.h \
    $$PWD/entitynotfoundexception.h \
    $$PWD/exception.h \
    $$PWD/graph.h \
    $$PWD/relation.h \
    $$PWD/relationexistexception.h \
    $$PWD/relationnotfoundexception.h

SOURCES += \
    $$PWD/Allocator.cpp \
    $$PWD/entity.cpp \
    $$PWD/entityexistexception.cpp \
    $$PWD/entitynotfoundexception.cpp \
    $$PWD/exception.cpp \
    $$PWD/graph.cpp \
    $$PWD/relation.cpp \
    $$PWD/relationexistexception.cpp \
    $$PWD/relationnotfoundexception.cpp
