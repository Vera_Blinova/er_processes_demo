#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <string>
#include <exception>
#include <sstream>

using namespace std;

class Exception : public exception
{
public:
    Exception();
    Exception(std::string text);

    virtual const char* what() const throw();

private:

    string fullString;
};

#endif // EXCEPTION_H
