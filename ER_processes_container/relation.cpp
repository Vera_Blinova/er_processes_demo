#include "relation.h"

Relation::Relation(string name, string ent1, string ent2, RelTy type)
{
    Name = name;
    Entity1 = ent1;
    Entity2 = ent2;
    Type = type;
}

string Relation::serialize()
{
    string str = "\t\t{ \n \t\t\t\"name\": \"" + Name + "\",\n " +
                          "\t\t\t\"entity1\": \"" + Entity1 + "\",\n" +
                          "\t\t\t\"entity2\": \"" + Entity2 + "\",\n" +
                          "\t\t\t\"type\": \"";
    string rt = serializeType();

    str += rt + "\"\n\t\t}";
    return str;
}

int Relation::GetIndexOfType()
{
    switch (Type)
    {
    case RelTy::T_1_1:
        return 0;

    case RelTy::T_1_M:
        return 1;

    case RelTy::T_M_M:
        return 2;

    }

}

string Relation::serializeType()
{
    switch (Type)
    {
    case RelTy::T_1_1:
        return "1:1";

    case RelTy::T_1_M:
        return "1:M";

    case RelTy::T_M_M:
        return "M:M";

    }
}
