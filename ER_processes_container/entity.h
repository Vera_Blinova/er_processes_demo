#ifndef ENTITY_H
#define ENTITY_H
using namespace std;
#include <map>
#include <string>


template <typename T>
class Entity
{
    string _name;
    T* value;
public:
    string getName() {return  _name; }
    void setName(string name) { _name=name; }

    T* Value() {return value; }

    map<string, Entity<T>*> Relations_1_1;
    map<string, Entity<T>*> Relations_1_M;
    map<string, Entity<T>*> Relations_M_1;

    string serialize();
    Entity(string name, T data);
    ~Entity();

};


#include "entity.cpp"
#endif // ENTITY_H
