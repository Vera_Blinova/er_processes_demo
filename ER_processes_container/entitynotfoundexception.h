#ifndef ENTITYNOTFOUNDEXCEPTION_H
#define ENTITYNOTFOUNDEXCEPTION_H

#include <string>
#include "Exception.h"

using namespace std;

class EntityNotFoundException : public Exception
{
private:
    string entityName;
    string message;
public:
    EntityNotFoundException(string text);

    string EntityName() { return entityName; }
    string Message() { return message; }

    virtual const char* what() const throw();
};

#endif // ENTITYNOTFOUNDEXCEPTION_H
