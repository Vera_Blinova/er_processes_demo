TEMPLATE    = app
TARGET      = graph_view
CONFIG      += qt warn_on thread c++14
QT          += widgets core gui qml quick quickcontrols2

include(../src/quickqanava.pri)
include(../ER_processes_container/ERprocessescontainer.pri)

SOURCES     +=  main.cpp \
    appcore.cpp

OTHER_FILES +=  main.qml \
                RelationNode.qml

HEADERS += \
       appcore.h


RESOURCES   +=  qml.qrc


