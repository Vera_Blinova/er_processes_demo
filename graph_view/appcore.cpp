#include "appcore.h"
#include <QDebug>
AppCore::AppCore(QObject *parent) : QObject(parent)
{
    myGraph = new GraphER<std::string>();
}

AppCore::~AppCore()
{
    delete(myGraph);
}

void AppCore::setGraphQML(QPointer<Graph> graphQan)
{
    myGraphQML = graphQan;
}

void AppCore::addEntity(QString name, QString data)
{
    try {
        myGraph->AddEntity(name.toStdString(), data.toStdString());

    } catch (EntityExistException e) {
        emit errorThrowed(QString::fromStdString(e.Message()));
        return;
    }

}

void AppCore::addRelation(QString name, QString entity1, QString entity2, int rt)
{
    RelTy relTy;
    switch (rt)
    {
    case 0:
        relTy = RelTy::T_1_1;
        break;
    case 1:
        relTy = RelTy::T_1_M;
        break;
    case 2:
        relTy = RelTy::T_M_M;
        break;
    default:
        relTy = RelTy::T_1_1;
    }

    try {
        myGraph->AddRelation(name.toStdString(), entity1.toStdString(), entity2.toStdString(), relTy);
    } catch (RelationExistException e) {
        emit errorThrowed(QString::fromStdString(e.Message()));
        return;
    } catch (EntityNotFoundException e) {
        emit errorThrowed(QString::fromStdString(e.Message()));
        return;
    }

}

void AppCore::renameEntity(QString oldName, QString newName)
{
    try {
        myGraph->RenameEntity(oldName.toStdString(), newName.toStdString());
    } catch (EntityExistException e) {
        emit errorThrowed(QString::fromStdString(e.Message()));
    } catch (EntityNotFoundException e) {
        emit errorThrowed(QString::fromStdString(e.Message()));
    }

}

void AppCore::renameRelation(QString oldName, QString newName)
{
    try{
        myGraph->RenameRelation(oldName.toStdString(),newName.toStdString());
    } catch (RelationExistException e) {
        emit errorThrowed(QString::fromStdString(e.Message()));
    } catch (RelationNotFoundException e) {
        emit errorThrowed(QString::fromStdString(e.Message()));
    }
}
void AppCore::removeRelation(QString name)
{
    try {
        myGraph->RemoveRelation(name.toStdString());
    } catch (RelationNotFoundException e) {
        emit errorThrowed(QString::fromStdString(e.Message()));
    }

}

void AppCore::save(QString url)
{
    QString str = QString::fromStdString(myGraph->serialize());
    url = url.right(url.length()-8);

    QFile file(url);
        // Trying to open in WriteOnly and Text mode
        if(!file.open(QFile::WriteOnly |
                      QFile::Text))
        {
            qDebug() << " Could not open file for writing";
            return;
        }

        QTextStream out(&file);
        out.setCodec("UTF-8");
        out << str;
        file.flush();
        file.close();

}

void AppCore::load(QString url)
{
      url = url.right(url.length()-8);

      QFile file(url);
        if(!file.open(QFile::ReadOnly |
                      QFile::Text))
        {
            qDebug() << " Could not open file for writing";
            return;
        }

        QString str = file.readAll();
        myGraph = GraphER<std::string>::deserialize(str.toStdString());
        map<string, Entity<std::string>*> entities = myGraph->GetEntities();
        map<string, Relation*> relations = myGraph->GetRelations();
        for (map<string, Entity<std::string>*>::const_iterator it = entities.begin(); it != entities.end(); ++it)
            emit entityAdded(QString::fromStdString(it->first), "");
        for (map<string, Relation*>::const_iterator it = relations.begin(); it != relations.end(); ++it)
            emit relationAdded(QString::fromStdString(it->second->Name),
                               QString::fromStdString(it->second->Entity1),
                               QString::fromStdString(it->second->Entity2),
                               QString::fromStdString(it->second->serializeType()));
        emit redrawed();

        file.close();
}


void AppCore::removeEntity(QString name)
{
    try{
        myGraph->RemoveEntity(name.toStdString());
    } catch (EntityNotFoundException e){
        emit errorThrowed(QString::fromStdString(e.Message()));
    }
}
