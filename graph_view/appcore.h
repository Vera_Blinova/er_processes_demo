#ifndef APPCORE_H
#define APPCORE_H

#include <QObject>
#include <QString>
#include <string>
#include <vector>
#include <map>
#include <QtMath>
#include "../ER_processes_container/graph.h"
#include "../ER_processes_container/entityexistexception.h"
#include "../ER_processes_container/entitynotfoundexception.h"
#include "../ER_processes_container/relationexistexception.h"
#include "../ER_processes_container/relationnotfoundexception.h"
#include <QuickQanava>
#include <QFile>
#include <QQuickView>
#include <QVariantList>

using namespace qan;

class AppCore : public QObject
{
    Q_OBJECT
public:
    explicit AppCore(QObject *parent = nullptr);
    ~AppCore();
    void setGraphQML(QPointer<Graph> graph);

signals:
    void errorThrowed(QString message);
    void entityAdded(QString name, QString data);
    void relationAdded(QString relationName, QString entityName1, QString entityName2, QString relationType);
    void redrawed();

public slots:
    void addEntity(QString Name, QString entityData);
    void addRelation(QString name, QString entity1, QString entity2, int rt);
    void renameEntity(QString oldName, QString newName);
    void renameRelation(QString oldName, QString newName);
    void removeEntity(QString name);
    void removeRelation(QString name);
    void save(QString);
    void load(QString);


private:
    GraphER<std::string> *myGraph;
    QPointer<Graph> myGraphQML;

    void redraw();

};

#endif // APPCORE_H
