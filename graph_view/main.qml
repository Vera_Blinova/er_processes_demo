import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.3
//import QtQuick.Dialogs 1.3

import QtQuick.Controls.Material 2.1
import QtQuick.Layouts           1.3
import QtQuick.Shapes            1.0
import Qt.labs.platform          1.1

import QuickQanava 2.0 as Qan
import "qrc:/QuickQanava" as Qan
import "." as Qan



Window {
    id: window
    visible: true
    minimumHeight: 600
    minimumWidth: 800
    width: 1300
    height: 700
    property alias cb_relationTypeDisplayText: cb_relationType.displayText
    title: qsTr("ER Processes")

    MessageDialog {
        id: messageDialog
        visible: false
        title: "Внимание"
        text: "Все несохраненные изменения будут утерены."
        buttons: StandardButton.Ok | StandardButton.Cancel
        onAccepted: {
            loadDialog.open()
            visible = false
        }
    }

    ToolBar {
        id: toolBar
        x: 0
        y: 678
        width: 400
        height: 25
        position: ToolBar.Footer
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -3
        focusPolicy: Qt.NoFocus
        spacing: 0
        contentWidth: 41
        visible: true
        font.pointSize: 9
        RowLayout {
            anchors.fill: parent
            Label {
                id:status
                text: ""
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignLeft
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
        }

    }
    Qan.Graph {
        id: graph
        objectName : "graph"

        property var entities: []
        property var relations: []

        property var relationDelegate: Qt.createComponent( "qrc:/RelationNode.qml" )


        function redraw() {


            var len = graph.entities.length
            var windowCenter = Qt.point( ( window.contentItem.width) / 4.,
                                        ( window.contentItem.height) / 2. )



            for (var i = 0; i<len; i++)
            {

                var currentEntity = graph.entities[i]
                currentEntity.item.x = windowCenter.x + 200 * Math.cos(i*2*Math.PI/len)
                currentEntity.item.y = windowCenter.y - 200 * Math.sin(i*2*Math.PI/len)
            }

            len = graph.relations.length

            for (var i = 0; i<len; i++)
            {

                var currentRelation = graph.relations[i]
                currentRelation.item.x = (currentRelation.entity1.item.x + currentRelation.entity2.item.x)/2.
                currentRelation.item.y = (currentRelation.entity1.item.y + currentRelation.entity2.item.y)/2.

            }

            status.text = "Обновлено"

        }

        function insertRelation(name, type) {
            var relation = insertNode(relationDelegate)
            relation.label = qsTr(name + '\n' + type)
            return relation
        }


    }

    Connections {
        target: appCore
        onErrorThrowed:
        {
            status.text = message
        }
        onRedrawed:
        {
            graph.redraw()
        }

        onEntityAdded:
        {
            var entity = graph.insertNode()
            entity.label = name
            graph.entities.push(entity)

        }
        onRelationAdded:
        {
            var entity1
            var entity2
            var relation
            for (var i = 0; i<graph.entities.length; i++)
            {
                if(graph.entities[i].label === entityName1)
                    entity1 = graph.entities[i]
                if(graph.entities[i].label === entityName2)
                    entity2 = graph.entities[i]
            }
            for (var i = 0; i<graph.relations.length;i++)
            {
                if(graph.relations[i].name === relationName)
                    return
            }


            relation = graph.insertRelation(relationName,relationType)
            relation.relTy = relationType
            relation.entity1 = entity1
            relation.entity2 = entity2
            relation.name = relationName
            graph.insertEdge(entity1,relation)
            graph.insertEdge(relation,entity2)
            graph.relations.push(relation)

            graph.redraw()

        }

    }


    Column {
        id: column1
        x: 680
        width: 250
        height: 425
        anchors.top: parent.top
        anchors.topMargin: 8
        spacing: 0
        anchors.right: parent.right
        anchors.rightMargin: 271
        transformOrigin: Item.Center

        TextArea {
            id: textArea
            text: qsTr("Сущность:")
        }

        TextField {
            id: e_entityname
            y: 25
            width: 250
            placeholderText: "Название  сущности"
            onTextChanged: {
                var text0 = text.charAt(0).toUpperCase() + text.slice(1)
                text = text0
            }
        }

        Button {
            id: b_addEntity
            objectName: "b_addEntity"
            width: 250
            height: 40
            text: qsTr("Добавить")
            focusPolicy: Qt.TabFocus

            signal g_clicked(string s, string data)
            onClicked: {
                if (e_entityname.text === "")
                {
                    status.text = qsTr(e_entityname.placeholderText + " не может быть пустым")
                    return
                }
                g_clicked(e_entityname.text, "")

                for (var i = 0; i<graph.entities.length;i++)
                    if (graph.entities[i].label === e_entityname.text)
                        return
                var entity = graph.insertNode()
                entity.label = e_entityname.text
                graph.entities.push(entity)
                e_entityname.text =""

                graph.redraw()
            }
        }

        TextField {
            id: e_oldName
            width: 250
            placeholderText: "Текущее название сущности"
            onTextChanged: {
                var text0 = text.charAt(0).toUpperCase() + text.slice(1)
                text = text0
            }
        }

        TextField {
            id: e_newName
            width: 250
            text: qsTr("")
            placeholderText: "Новое название сущности"
            onTextChanged: {
                var text0 = text.charAt(0).toUpperCase() + text.slice(1)
                text = text0
            }
        }


        Button {
            id: b_renameEntity
            objectName: "b_renameEntity"
            width: 250
            text: qsTr("Переименовать")
            signal g_clicked(string oldName, string newName)
            onClicked: {
                if (e_oldName.text === "" )
                {
                    status.text = qsTr(e_oldName.placeholderText + " не может быть пустым")
                    return
                }
                if (e_newName.text === "")
                {
                    status.text = qsTr(e_newName.placeholderText + " не может быть пустым")
                    return
                }
                g_clicked(e_oldName.text,e_newName.text)

                for (var i = 0; i<graph.entities.length;i++)
                    if (graph.entities[i].label === e_newName.text)
                        return

                var entity
                for (var i = 0; i<graph.entities.length; i++)
                {
                    if(graph.entities[i].label === e_oldName.text)
                    {
                        entity = graph.entities[i]
                        break
                    }
                }

                if(!entity)
                    return

                entity.label = e_newName.text

                e_oldName.text = ""
                e_newName.text = ""

                graph.redraw()



            }
        }


        TextField {
            id: e_removeEntity
            width: 250
            text: qsTr("")
            placeholderText: "Название удаляемой сущности"
            onTextChanged: {
                var text0 = text.charAt(0).toUpperCase() + text.slice(1)
                text = text0
            }
        }

        Button {
            id: b_removeEntity
            objectName: "b_removeEntity"
            width: 250
            height: 40
            text: qsTr("Удалить")
            signal g_clicked(string name)

            onClicked: {
                if (e_removeEntity.text === "")
                {
                    status.text = qsTr(e_removeEntity.placeholderText + " не может быть пустым")
                    return
                }

                var entity
                for (var i = 0; i<graph.entities.length; i++)
                {
                    if(graph.entities[i].label === e_removeEntity.text)
                    {    entity = graph.entities[i]
                        break
                    }
                }

                if (!entity)
                    return


                for (var j = graph.relations.length - 1; j>=0; j--)
                {
                    if (graph.relations[j].entity1.label === e_removeEntity.text || graph.relations[j].entity2.label === e_removeEntity.text)
                    {
                        var relation = graph.relations[j]
                        graph.removeNode(relation)
                        graph.relations.splice(j, 1)


                    }
                }


                graph.removeNode(entity)
                graph.entities.splice(i, 1)

                g_clicked(e_removeEntity.text)

                e_removeEntity.text = ""

                graph.redraw()
            }
        }

     }

    Column {
        id: column2
        x: 940
        width: 250
        height: 400
        anchors.top: parent.top
        anchors.topMargin: 8
        anchors.right: parent.right
        anchors.rightMargin: 10
        TextArea {
            id: textArea1
            text: qsTr("Отношение:")
        }

        TextField {
            id: e_relationname
            width: 250
            text: qsTr("")
            placeholderText: "Название отношения"
            onTextChanged: {
                var text0 = text.charAt(0).toUpperCase() + text.slice(1)
                text = text0
            }
        }

        TextField {
            id: e_firstEntity
            width: 250
            text: qsTr("")
            placeholderText: "Название первой сущности"
            onTextChanged: {
                var text0 = text.charAt(0).toUpperCase() + text.slice(1)
                text = text0
            }
        }

        TextField {
            id: e_secondEntity
            width: 250
            text: qsTr("")
            placeholderText: "Название второй сущности"
            onTextChanged: {
                var text0 = text.charAt(0).toUpperCase() + text.slice(1)
                text = text0
            }
        }

        ComboBox {
            id: cb_relationType
            width: 250
            textRole: ""
            font.pointSize: 10
            font.family: "Arial"
            displayText: currentText
            editable: false
            focusPolicy: Qt.TabFocus
            spacing: -1
            currentIndex: 0
            model: ["1:1", "1:M"/*, "M:M"*/]
        }

        Button {
            id: b_addRelation
            objectName: "b_addRelation"
            width: 250
            height: 40
            text: qsTr("Добавить")
            focusPolicy: Qt.TabFocus
            signal g_clicked(string nameRelation, string nameEntity1, string nameEntity2, int relTy)

            onClicked: {
                if (e_relationname.text === "")
                {
                    status.text = qsTr(e_relationname.placeholderText + " не может быть пустым")
                    return
                }
                if (e_firstEntity.text === "" )
                {
                    status.text = qsTr(e_firstEntity.placeholderText + " не может быть пустым")
                    return
                }
                if (e_secondEntity.text === "")
                {
                    status.text = qsTr(e_secondEntity.placeholderText + " не может быть пустым")
                    return
                }
                g_clicked(e_relationname.text,e_firstEntity.text,e_secondEntity.text,cb_relationType.currentIndex)

                var entity1
                var entity2
                var relation
                for (var i = 0; i<graph.entities.length; i++)
                {
                    if(graph.entities[i].label === e_firstEntity.text)
                        entity1 = graph.entities[i]
                    if(graph.entities[i].label === e_secondEntity.text)
                        entity2 = graph.entities[i]
                }
                if (!entity1)
                    return
                if(!entity2)
                    return
                for (var i = 0; i<graph.relations.length;i++)
                {
                    if(graph.relations[i].name === e_relationname.text)
                        return
                }


                relation = graph.insertRelation(e_relationname.text,cb_relationType.currentText)
                relation.relTy = cb_relationType.currentText
                relation.entity1 = entity1
                relation.entity2 = entity2
                relation.name = e_relationname.text
                graph.insertEdge(entity1,relation)
                graph.insertEdge(relation,entity2)
                graph.relations.push(relation)

                e_relationname.text = ""
                e_firstEntity.text = ""
                e_secondEntity.text = ""
                cb_relationType.currentIndex = 0

                graph.redraw()


            }

        }

        TextField {
            id: e_oldRelName
            width: 250
            text: qsTr("")
            placeholderText: "Текущее название отношения"
            onTextChanged: {
                var text0 = text.charAt(0).toUpperCase() + text.slice(1)
                text = text0
            }
        }

        TextField {
            id: e_newRelName
            width: 250
            placeholderText: "Новое название отношения"
            onTextChanged: {
                var text0 = text.charAt(0).toUpperCase() + text.slice(1)
                text = text0
            }
        }


        Button {
            id: b_renameRelation
            objectName: "b_renameRelation"
            width: 250
            text: qsTr("Переименовать")
            signal g_clicked(string oldName,string newName)

            onClicked: {
                if (e_oldRelName.text === "" )
                {
                    status.text = qsTr(e_oldRelName.placeholderText + " не может быть пустым")
                    return
                }
                if (e_newRelName.text === "")
                {
                    status.text = qsTr(e_newRelName.placeholderText + " не может быть пустым")
                    return
                }
                g_clicked(e_oldRelName.text, e_newRelName.text)

                for (var i = 0; i<graph.relations.length;i++)
                    if (graph.relations[i].name === e_newRelName.text)
                        return

                var relation
                for (var i = 0; i<graph.relations.length; i++)
                {
                    if(graph.relations[i].name === e_oldRelName.text)
                    {
                        relation = graph.relations[i]
                        break
                    }
                }

                if(!relation)
                    return
                relation.name = e_newRelName.text
                relation.label = qsTr(e_newRelName.text + '\n' + relation.relTy)

                e_oldRelName.text = ""
                e_newRelName.text = ""

                graph.redraw()
            }
        }

        TextField {
            id: e_removeRelation
            width: 250
            text: qsTr("")
            placeholderText: "Название удаляемого отношения"
            onTextChanged: {
                var text0 = text.charAt(0).toUpperCase() + text.slice(1)
                text = text0
            }
        }

        Button {
            id: b_removeRelation
            objectName: "b_removeRelation"
            width: 250
            height: 40
            text: qsTr("Удалить")
            signal g_clicked(string name)

            onClicked: {
                if (e_removeRelation.text === "" )
                {
                    status.text = qsTr(e_removeRelation.placeholderText + " не может быть пустым")
                    return
                }

                g_clicked(e_removeRelation.text)

                var relation
                for (var i = 0; i<graph.relations.length; i++)
                {
                    if(graph.relations[i].name === e_removeRelation.text)
                    {
                        relation = graph.relations[i]
                        break
                    }
                }
                if (!relation)
                    return
                graph.relations.splice(i, 1)
                graph.removeNode(relation)
                e_removeRelation.text =""

                graph.redraw()
            }
        }

    }

    Text {
        id: element
        x: 416
        y: 518
        text: qsTr("")
        lineHeight: 0
        font.pixelSize: 12
    }

    FileDialog {
        id: fileDialog
        fileMode: FileDialog.SaveFile
        defaultSuffix: ".json"
        nameFilters: ["JSON files (*.json)"]
        onAccepted: {
            btn_save.file = currentFile
        }
       }
    FileDialog {
        id: loadDialog
        fileMode:  FileDialog.OpenFile
        defaultSuffix: ".json"
        nameFilters: ["JSON files (*.json)"]
        onAccepted: {
            graph.entities = []
            graph.relations = []
            graph.qmlClearGraph()
            btn_load.file = currentFile
        }
    }

    Button {
        id: btn_save
        objectName: "b_save"
        x: 1040
        y: 646
        width: 250
        height: 40
        text: qsTr("Сохранить")
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 14
        focusPolicy: Qt.NoFocus
        property var file: ""
        signal g_clicked(string url)
        onClicked: {
            fileDialog.open()
        }
        onFileChanged: {
            g_clicked(file)
            status.text = qsTr("Модель сохранена")
        }
    }

        Button {
            id: btn_load
            objectName: "b_load"
            x: 780
            y: 646
            width: 250
            height: 40
            text: qsTr("Загрузить")
            property var file: ""
            signal g_clicked(string url)
            onClicked: {
                messageDialog.visible = true
            }
            onFileChanged: {
                g_clicked(file)
                status.text = qsTr("Модель загружена")
            }
        }
    }


