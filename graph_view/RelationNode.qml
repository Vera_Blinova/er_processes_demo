import QtQuick              2.8
import QtQuick.Layouts      1.3
import QtQuick.Controls 2.3

import QuickQanava 2.0 as Qan
import "qrc:/QuickQanava" as Qan
import "." as Qan

Qan.NodeItem {
    id: relationNode
    Layout.preferredWidth: 100
    Layout.preferredHeight: 100
    width: Layout.preferredWidth
    height: Layout.preferredHeight

    property string relType: ""
    property var entity1: undefined
    property var entity2: undefined
    property var name: undefined

    property var symbolPolygon: new Array(5)
    complexBoundingShape: true                  // SAMPLE: Do not forget to set complexBoundingShape to true for non
    onRequestUpdateBoundingShape: {             // rectangular node otherwise requestUpdateBoundingShape() will never be
        var w = width - 1                       // called (an a rectangular BS will be generated more efficietly from c++)
        var w2 = w / 2
        var h = height - 1
        var h2 = h / 2
        symbolPolygon[0] = Qt.point( w2, 1 )
        symbolPolygon[1] = Qt.point( w2 + h2, h2 )
        symbolPolygon[2] = Qt.point( w2, h )
        symbolPolygon[3] = Qt.point( w2 - h2, h2 )
        symbolPolygon[4] = Qt.point( w2, 1 )
        setBoundingShape( symbolPolygon )
    }

    Qan.CanvasNodeTemplate {
        id: template
        anchors.fill: parent
        nodeItem : relationNode
        symbol: Canvas {
            anchors.fill: parent
            z: 1
            id: nodeSymbol
            onPaint: {
                var ctx = nodeSymbol.getContext( "2d" )
                ctx.clearRect( 0, 0, width, height )
                ctx.lineWidth = relationNode.borderWidth
                ctx.strokeStyle = relationNode.borderColor
                ctx.beginPath( )
                var w = width - 1;  var w2 = w / 2
                var h = height - 1; var h2 = h / 2
                ctx.moveTo( w2, 1 )
                ctx.lineTo( w2 + h2, h2 )
                ctx.lineTo( w2, h )
                ctx.lineTo( w2 - h2, h2 )
                ctx.lineTo( w2, 1 )
                ctx.stroke( )
                var gradient = ctx.createLinearGradient(0, 0, width, height);
                gradient.addColorStop(0.1, Qt.lighter( relationNode.style.backColor, 1.8 )  );
                gradient.addColorStop(0.9, relationNode.style.backColor );
                ctx.fillStyle = gradient
                ctx.fill( )
            }
        }
    }
}
