// Qt headers
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QQuickStyle>
#include <QPointer>
#include <QFileDialog>
#include  <QDebug>
// Custom headers
#include <QuickQanava>
#include "../ER_processes_container/graph.h"
#include "appcore.h"

using namespace qan;

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    AppCore appCore;
    QQmlContext *context = engine.rootContext();
    context->setContextProperty("appCore", &appCore);

    QQuickStyle::setStyle("Material");

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    QuickQanava::initialize(&engine);

    engine.load(url);

    //Получение ссылок на объекты интерфейса
    QPointer<Graph> graph ;
    QObject* btn_addEntity;
    QObject* btn_addRelation;
    QObject* btn_renameEntity;
    QObject* btn_renameRelation;
    QObject* btn_removeEntity;
    QObject* btn_removeRelation;
    QObject* btn_save;
    QObject* btn_load;

    
    for (const auto rootObject : engine.rootObjects()) {
        graph = qobject_cast<Graph*>(rootObject->findChild<QQuickItem *>("graph"));
        btn_addEntity = qobject_cast<QObject*>(rootObject->findChild<QQuickItem *>("b_addEntity"));
        btn_addRelation = qobject_cast<QObject*>(rootObject->findChild<QQuickItem *>("b_addRelation"));
        btn_removeEntity = qobject_cast<QObject*>(rootObject->findChild<QQuickItem *>("b_removeEntity"));
        btn_removeRelation = qobject_cast<QObject*>(rootObject->findChild<QQuickItem *>("b_removeRelation"));
        btn_renameEntity = qobject_cast<QObject*>(rootObject->findChild<QQuickItem *>("b_renameEntity"));
        btn_renameRelation = qobject_cast<QObject*>(rootObject->findChild<QQuickItem *>("b_renameRelation"));
        btn_save = qobject_cast<QObject*>(rootObject->findChild<QQuickItem *>("b_save"));
        btn_load = qobject_cast<QObject*>(rootObject->findChild<QQuickItem *>("b_load"));
    }

    // соединение QML интерфейса и ERprocesses контейнером
    QObject::connect(btn_addEntity, SIGNAL(g_clicked(QString,QString)), &appCore, SLOT(addEntity(QString,QString)));
    QObject::connect(btn_renameEntity, SIGNAL(g_clicked(QString,QString)), &appCore, SLOT(renameEntity(QString,QString)));
    QObject::connect(btn_removeEntity, SIGNAL(g_clicked(QString)), &appCore, SLOT(removeEntity(QString)));

    QObject::connect(btn_addRelation, SIGNAL(g_clicked(QString,QString,QString,int)), &appCore, SLOT(addRelation(QString,QString,QString,int)));
    QObject::connect(btn_renameRelation, SIGNAL(g_clicked(QString,QString)), &appCore, SLOT(renameRelation(QString,QString)));
    QObject::connect(btn_removeRelation, SIGNAL(g_clicked(QString)), &appCore, SLOT(removeRelation(QString)));

    QObject::connect(btn_save, SIGNAL(g_clicked(QString)), &appCore, SLOT(save(QString)));
    QObject::connect(btn_load, SIGNAL(g_clicked(QString)), &appCore, SLOT(load(QString)));

    appCore.setGraphQML(graph);

    return app.exec();
}
